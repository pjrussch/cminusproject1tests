#!/bin/bash
passed=$(expr 0)
for i in input/*.cm; do
    echo "Interpreting $i"

    if [ -f $( echo $i | cut -d'.' -f 1,2 )".in" ]; then
        ./cmc $i < $( echo $i | cut -d'.' -f 1,2 )".in"
    else
        ./cmc $i
    fi

    echo "Result: "
    sFile=$( echo $i | cut -d'.' -f 1,2 )".s"
    oFile="input/output/"$( echo $i | cut -d'/' -f 2 | cut -d'.' -f 1,2 )".out"
    result=$(diff $oFile $sFile)
    if [ ! -z  "$result" ]; then
        echo "Fail"
        echo $result
    else 
        echo "Pass"
        passed=$(($passed + 1))
    fi
    echo ""
done;

echo "Cleaning .s files"
for i in input/*.s; do
    rm -f $i
done;

echo "Passed test cases: $passed"
